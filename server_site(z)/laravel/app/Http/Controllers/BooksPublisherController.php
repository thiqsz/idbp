<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use View;
use Response;
use File;
use Request;

class BooksPublisherController extends Controller {
		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
		public function index()
		{
	      	$publisher = Publishers::all();
	  		return View::make('index') 
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.publisher', array('publisher' => $publisher))
	      ->nest('footer_script', 'footer_script_datatables');
			}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
				return View::make('index')  
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.publisher.create')
	      ->nest('footer_script', 'footer_script_forms');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
        if (Request::ajax()) {
              $rules = array(
	             'publisher_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/publisher/create')
	                ->withErrors($validator);
	        } else {
	            $publisher = new Publishers;
	            $publisher->publisher_name = Input::get('publisher_name');
	            $publisher->save();
                $result = [
                'pubname' => Input::get('publisher_name'),
                'pubid' => $author->pub_id
                ];
                   return Response::json($result);
	       }
    

         }else{
	        $rules = array(
	            'publisher_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/publisher/create')
	                ->withErrors($validator);
	        } else {
	            $publisher = new Publishers;
	            $publisher->publisher_name = Input::get('publisher_name');
	            $publisher->save();

	            // redirect
	            Session::flash('flash_message', 'Data baru bejaya disimpan');
	            Session::flash('flash_type', 'alert-success');
	            return Redirect::to('books/publisher');
	            }
            }
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
      $publisher = Publishers::find($id);
      if (is_null($publisher))
      {
        return Redirect::to('books/publisher');
      }
      return View::make('index')
      ->nest('header_script', 'header_script')
      ->nest('side_menu', 'side_menu')
      ->nest('body', 'books.publisher.edit', array('publisher' => $publisher))
      ->nest('footer_script', 'footer_script_datatables');
        
		}
		

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{

      $rules = array(
        'publisher_name'=> 'required'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
        return Redirect::to('books/publisher/' . $id . '/edit')
        ->withErrors($validator);
      } else {
        // store
        $publisher = Publishers::find($id);
        $publisher->publisher_name= Input::get('publisher_name');
        $publisher->save();
        // redirect
        Session::flash('flash_message', 'Proses kemaskini berjaya');
        Session::flash('flash_type', 'alert-success');
        return Redirect::to('books/publisher');
      }
    }
		

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//delete category
      Publishers::find($id)->delete();
      Session::flash('flash_message', 'Data berjaya dipadam.');
      Session::flash('flash_type', 'alert-success');
      return Redirect::to('books/publisher');
		}
	    
	    

	}