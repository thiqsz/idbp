<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use View;
use Response;
use File;
use Request;

class BooksAuthorController extends Controller {
		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
		public function index()
		{
	       $author = Author::all();
	  		return View::make('index') 
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.author', array('author' => $author))
	      ->nest('footer_script', 'footer_script_datatables');
			}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
			return View::make('index')  
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.author.create')
	      ->nest('footer_script', 'footer_script_forms');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
        if (Request::ajax()) {
              $rules = array(
	            'author_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/author/create')
	                ->withErrors($validator);
	        } else {
	            $author = new Author;
	            $author->author_name = Input::get('author_name');
	            $author->save();
                $result = [
                'authorname' => Input::get('author_name'),
                'authorid' => $author->author_id
                ];
                   return Response::json($result);
	       }
    

         }else{
	        $rules = array(
	            'author_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/author/create')
	                ->withErrors($validator);
	        } else {
	            $author = new Author;
	            $author->author_name = Input::get('author_name');
	            $author->save();
	            // redirect
	            Session::flash('flash_message', 'Successfully insert new author');
	            Session::flash('flash_type', 'alert-success');
	            return Redirect::to('books/author');
	            }
		}
        }

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
      //get the current book by id
      // $category = Category::where('cat_id', $cat_id)->get();
			$author = Author::find($id);
      if (is_null($author))
      {
        return Redirect::to('books/author');
      }
      // redirect to update form
      return View::make('index')
      ->nest('header_script', 'header_script')
      ->nest('side_menu', 'side_menu')
      ->nest('body', 'books.author.edit', array('author' => $author))
      ->nest('footer_script', 'footer_script_datatables');
        
		}
		

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{

      $rules = array(
        'author_name'=> 'required'
      );
      $validator = Validator::make(Input::all(), $rules);

      // process the login
      if ($validator->fails()) {
        return Redirect::to('books/author/' . $id . '/edit')
        ->withErrors($validator);
      } else {
        // store
        $author = Author::find($id);
        $author->author_name= Input::get('author_name');
        $author->save();
        // redirect
        Session::flash('flash_message', 'Successfully update author');
        Session::flash('flash_type', 'alert-success');
        return Redirect::to('books/author');
      }
    }
        
		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//delete category
      Author::find($id)->delete();
      Session::flash('flash_message', 'Successfully delete author');
      Session::flash('flash_type', 'alert-success');
      return Redirect::to('books/author');
		}
	    
	    

	}