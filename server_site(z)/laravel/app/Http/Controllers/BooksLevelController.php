<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use View;
use Response;
use File;
use Request;

class BooksLevelController extends Controller {
		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
		public function index()
		{
	      	$level = Booklevel::all();
	  		return View::make('index') 
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.level', array('level' => $level))
	      ->nest('footer_script', 'footer_script_datatables');
			}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
				return View::make('index')  
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.level.create')
	      ->nest('footer_script', 'footer_script_forms');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{ 
            if (Request::ajax()) {
              $rules = array(
	             'level_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/level/create')
	                ->withErrors($validator);
	        } else {
	            $level = new Booklevel;
	            $level->level_name = Input::get('level_name');
	            $level->save();
                $result = [
                'levelname' => Input::get('level_name'),
                'levelid' => $author->level_id
                ];
                   return Response::json($result);
	       }
    

         }else{
	        $rules = array(
	            'level_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/level/create')
	                ->withErrors($validator);
	        } else {
	            $level = new Booklevel;
	            $level->level_name = Input::get('level_name');
	            $level->save();

	            // redirect
	            Session::flash('flash_message', 'Data baru bejaya disimpan');
	            Session::flash('flash_type', 'alert-success');
	            return Redirect::to('books/level');
	            }
            }
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
      //get the current book by id
      // $category = Category::where('cat_id', $cat_id)->get();
			$level = Booklevel::find($id);
      if (is_null($level))
      {
        return Redirect::to('books/level');
      }
      // redirect to update form
      return View::make('index')
      ->nest('header_script', 'header_script')
      ->nest('side_menu', 'side_menu')
      ->nest('body', 'books.level.edit', array('level' => $level))
      ->nest('footer_script', 'footer_script_datatables');
        
		}
		

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{
			// validate
      // read more on validation at http://laravel.com/docs/validation
      $rules = array(
        'level_name'=> 'required'
      );
      $validator = Validator::make(Input::all(), $rules);

      // process the login
      if ($validator->fails()) {
        return Redirect::to('books/level/' . $id . '/edit')
        ->withErrors($validator);
      } else {
        // store
        $level = Booklevel::find($id);
        $level->level_name= Input::get('level_name');
        $level->save();
        // redirect
        Session::flash('flash_message', 'Proses kemaskini berjaya');
        Session::flash('flash_type', 'alert-success');
        return Redirect::to('books/level');
      }
    }
		

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//delete category
      Booklevel::find($id)->delete();
      Session::flash('flash_message', 'Data berjaya dipadam.');
      Session::flash('flash_type', 'alert-success');
      return Redirect::to('books/level');
		}
	    
	    

	}