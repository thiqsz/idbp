<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use App\DBPStatus;
use App\BooksImages;
use View;
use Response;
use File;
use Request;
use Auth;
use DB;
use GuzzleHttp\Url;
use Storage;
use Hash;
// use Illuminate\Contracts\Routing\ResponseFactory;

class BooksBookController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $name = Auth::user()->name;

        $books = Books::with('categories','authors')->get();
        
        $books = $this->simplyBooksAuthorsAndCategories($books);

        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book', array('books' => $books))
            ->nest('footer_script', 'footer_script_datatables')
            ->with('name', $name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $authors = Author::lists('author_name', 'author_id');
        $category = Category::lists('category_name', 'cat_id');
        $booklevel = BookLevel::lists('level_name', 'level_id');
        $publishers = Publishers::lists('publisher_name', 'pub_id');
        $status = DBPStatus::lists('status_name', 'status_id');
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book.create', array('authors' => $authors, 'category' => $category, 'booklevel' => $booklevel, 'publishers' => $publishers, 'status' => $status))
            ->nest('footer_script', 'footer_script_forms');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $rules = array(
            'title' => 'required',
            //'price' => 'required',
            'author' => 'required',
            // 'category' => 'required',
            // 'isbn' => 'required',
            //'year' => 'required',
            //'pages' => 'required',
            // 'booklevel' => 'required',
            // 'bookcover' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            // 'bookcontentepub' => 'required|mimes:epub',
            // 'bookcontent' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            //'summary' => 'required'
        );


        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('books/book/create')
                ->withErrors($validator);
        } else {
            $book = new Books;
            // if (Input::file('bookcover')->isValid()) {


                        
                $book->title = Input::get('title');
                $book->price = Input::get('price');
                // $book->authors =  $authors;
                // $book->cat_id = Input::get('category');
                $book->pub_id = Input::get('publisher');
                $book->isbn = Input::get('isbn');
                $book->level = Input::get('booklevel');
                $book->year = Input::get('year');
                $book->pages = Input::get('pages');
                $book->summary = Input::get('summary');
                $book->status = Input::get('status');

                $titlename = preg_replace('/[^A-Za-z0-9\-]/', '', $book->title);

                if( Input::file('bookcover')){
                    $book = $this->storeBookCover(Input::file('bookcover'),$book);
                }

                
                
                if($book->save()){
                    //sync to relationship
                    if(Input::file('bookcontentepub')){
                        $book=$this->storeContent('epub',Input::file('bookcontentepub'),$book);
                    }
                    
                    if(Input::file('bookcontentpdf')){
                        $book=$this->storeContent('pdf',Input::file('bookcontentpdf'),$book);
                    }
                    $book->save();
                    $book->authors()->sync(Input::get('author'));
                    $book->categories()->sync(Input::get('category'));
                    // samples
                    $tempBooks=BooksImages::where('temp','=',csrf_token())
                        ->get();
                    $destinationPath = 'images/bookcover/'.$book->id.'/preview/';
                    foreach ($tempBooks as $temp) {
                        $path = explode("/", $temp->images_path);
                        $temp->temp =null;
                        $temp->books_id = $book->id;
                        $oriPath = $temp->images_path;
                        $temp->images_path = $destinationPath.$path[sizeof($path)-1];
                        $destiny = public_path().'/'.$destinationPath;
                        if(!file::exists($destiny)){
                            file::makeDirectory($destiny, 0777, true, true);
                        }

                        if(File::move(public_path().'/'.$oriPath,$destiny.$path[sizeof($path)-1])){
                           $temp->save();
                        }
                    }
                     Session::flash('message', 'Successfully add new book!');
                        return Redirect::to('books/book');

                }else{

                    Session::flash('message', 'Fail to create new book!');
                    return Redirect::to('books/book/create');
                }
            }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // get the book
        $books = Books::find($id);
        if (is_null($books)) {
            return Redirect::to('books/book');
        }
        $books = Books::with('categories','authors')
                ->where('id','=',$id)->get();
      
        $books = $this->simplyBooksAuthorsAndCategories($books);


        // show the view and pass the books to it
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book.show', array('books' => $books[0]))
            ->nest('footer_script', 'footer_script_forms');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        $books = Books::with('authors')
        ->where('id',"=",$id)
        ->get();
        $books = $books[0];

        $authors = array();
        $categories = array();
      
        foreach ($books->authors as $auth) {
            array_push($authors,$auth->author_id);
        }
        foreach ($books->categories as $cat) {
            array_push($categories,$cat->cat_id);
        }

        $books['authors_id']= $authors;
        $books['categories_id']= $categories;

        $bookimages = DB::table('dbp_books_images')->where('books_id', '=', $id)->get();

        if (is_null($books)) {
            return Redirect::to('books/book');
        }

        if (isset($books->status)) {
        }

        $authors = Author::lists('author_name', 'author_id');
        $category = Category::lists('category_name', 'cat_id');
        $booklevel = BookLevel::lists('level_name', 'level_id');
        $publishers = Publishers::lists('publisher_name', 'pub_id');
        $status = DBPStatus::lists('status_name', 'status_id');
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book.edit', array(
                'authors' => $authors, 
                'category' => $category, 
                'booklevel' => $booklevel, 
                'publishers' => $publishers, 
                'books' => $books, 
                'status' => $status, 
                'bookimages' => $bookimages))
            ->nest('footer_script', 'footer_script_forms');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'title' => 'required',
            'price' => 'required',
            'author' => 'required',
            'category' => 'required',
            // 'isbn' => 'required',
            'year' => 'required',
            'pages' => 'required',
            // 'booklevel' => 'required',
            // 'bookcover' => 'image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'summary' => 'required',
            'bookcontentepub' => 'mimes:epub',
            'bookcontentpdf' => 'mimes:pdf'

        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('books/book/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
           
            $book = Books::find($id);

            $book->title = Input::get('title');
            $book->price = Input::get('price');
            $book->pub_id = Input::get('publisher');
            $book->isbn = Input::get('isbn');
            $book->level = Input::get('booklevel');
            $book->year = Input::get('year');
            $book->pages = Input::get('pages');
            $book->summary = Input::get('summary');
            $book->status = Input::get('status');

            if( Input::file('bookcover')){
                $book = $this->storeBookCover(Input::file('bookcover'),$book);
            }

            if(Input::file('bookcontentepub')){
                $book=$this->storeContent('epub',Input::file('bookcontentepub'),$book);
            }
            
            if(Input::file('bookcontentpdf')){
                $book=$this->storeContent('pdf',Input::file('bookcontentpdf'),$book);
            }

            if($book->save()){
                $book->authors()->sync(Input::get('author'));
                $book->categories()->sync(Input::get('category'));
            }

            // redirect
            Session::flash('message', 'Successfully updated book!');
            return Redirect::to('books/book');
        }
    }


    private function simplyBooksAuthorsAndCategories($books){
        foreach ($books as $book) {
           $authors = array();
           $categories = array();

            foreach($book->authors as $author){
                array_push($authors, $author->author_name);
                 
            }
            foreach($book->categories as $cat){
                array_push($categories, $cat->category_name);
            }
           unset($book['authors']);
           unset($book['categories']);
           $book['authors'] = $authors;
           $book['categories'] = $categories;
        }

        return $books;
    }

    private function simplyBooksAuthorsAndSamples($book){
        $samples = array();
        $authors = array();
        $categories = array();
       
        foreach ($book->samples as $sample) {
           array_push($samples, $sample->images_path);
        }

        foreach ($book->authors as $author) {
           array_push($authors, $author->author_name);
        }

        foreach($book->categories as $cat){
            array_push($categories, $cat->category_name);
        }

        unset($book['authors']);
        unset($book['samples']);
        unset($book['categories']);
        $book['authors'] = $authors;
        $book['samples'] = $samples;
        $book['categories'] = $categories;

        return $book;

    }

    private function storeContent($case,$input,$book){

         
        $bookcontent = $case.'-'.$input->getClientOriginalName();
        $destinationPath4Content = storage_path() . '/app/downloads/books/' . $book->id.'/' ;
        $bookContentDest = $destinationPath4Content . $bookcontent;

        if($input->move($destinationPath4Content, $bookcontent)){
            switch ($case) {
                            case 'epub':
                                $book->epub = $bookcontent;
                                break;
                            case 'pdf':
                                $book->pdf = $bookcontent;
                                break;
                            default:
                                # code...
                                break;
                        }          
         
            return $book;
        }
    
            

    }

    private function storeBookCover($input,$book){
        $bookname = Input::file('bookcover')->getClientOriginalName();
        $titlename = preg_replace('/[^A-Za-z0-9\-]/', '', $book->title); ///////////////!!!!!change to random
        $extension=explode("/",Input::file('bookcover')->getMimeType())[1]; ///////////////!!!!! add new line
        $destinationPath = 'images/bookcover/' . $book->id ; ///////////////!!!!!change to ID
        $bookDestination = $destinationPath.'/' . strtolower(preg_replace('/\s+/', '', $titlename )).'.'.$extension;
        if (File::exists($destinationPath)) {
        //     File::delete($destinationPath);
            File::makeDirectory($destinationPath, 0777, true, true);
        }
        
        Request::File('bookcover')->move($destinationPath, $titlename .'.'.$extension);
        $book->pic = $bookDestination;

        return $book;
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //delete books
        $book=Books::find($id);
        $book->authors()->detach();
        $book->samples()->delete();
        $book->categories()->detach();
        $book->delete();
        return Redirect::to('books/book')
            ->withInput()
            ->with('message', 'Successfully deleted Book.');
    }



    public function updateUploadSample($id){

        //check request rules
        $rules = array(
             'files' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
        );


       
        $validator = Validator::make(Input::all(), $rules);
        // request is JQuery and valid post
        if(is_numeric($id)){
            if(Request::ajax() && !$validator->fails()){
                    $destinationPath = 'images/bookcover/'.$id.'/preview/';
                    $name =  Request::file('files')->getClientOriginalName();
                    
                    //save file
                    if(Request::file('files')->move($destinationPath,$name)){
                        //db
                        $book = Books::find($id);

                        $sample = new BooksImages();
                        $sample->images_path=$destinationPath.$name;
                        $status=$book->samples()->save($sample);
                
                        //return file path
                        return response()->json(array('msg'=>'file uploaded','saved'=>$status,'path'=>$destinationPath.$name))//$destinationPath.$name)
                        ->setCallback(Request::input('callback'));
                    }

                   
            }
        }else{

            if(Request::ajax() && !$validator->fails()){
                $destinationPath = 'images/bookcover/temp/preview/';
                $name =  Request::file('files')->getClientOriginalName();
                
                //save file
                if(Request::file('files')->move($destinationPath,$name)){
                    $sample = new BooksImages();
                    $sample->images_path=$destinationPath.$name;
                    $sample->temp=$id;
                    if($status=$sample->save()){
                        return response()->json(array('msg'=>'file saved to temp'))//$destinationPath.$name)
                                ->setCallback(Request::input('callback'));
                    }
            
                    
                }
            }
            
        }
        
        return App::abort(501, 'Feature not implemented');
        
    }

    public function deleteAllUploadedSample($id){
        //find id
        $book =  Books::find($id);
        $destinationPath = 'images/bookcover/'.$id.'/preview/';
        if($status= $book->samples()->delete()){
            //Delete all files
             File::deleteDirectory(public_path().'/'.$destinationPath);
             return response()->json(array('msg'=>'all files deleted','status'=>'OK'))
                    ->setCallback(Request::input('callback'));
        }

        return response()->json(array('status'=>'0'))
                    ->setCallback(Request::input('callback'));
       
    }


    public function downloadBook($id,$format){

        $book=Books::findOrFail($id);
        $destinationPath4Content = 'downloads/books/' . $book->id.'/' ;
        $fileType =  array('pdf'=>$book->pdf,'epub'=>$book->epub);
    
        if(array_key_exists($format,$fileType) && $fileType[$format]!==null && Storage::disk('local')->exists($destinationPath4Content.$fileType[$format])){
            return response()->download(storage_path().'/app/'.$destinationPath4Content.$fileType[$format]);
        }

       
    }

    
   

    public function categorylist($catlist)
    {
        foreach ($catlist as $list) {

            $bookcatlist = $list->category_name;
            return $bookcatlist;
        }

    }

    public function booklist($catlist)
    {

        foreach ($catlist as $list) {
            $bookarray = Books::where('cat_id', $list->cat_id)->get();


           // var_dump($books);
            foreach($bookarray as $booksa) {
                $books = $this->bookcontentsample($booksa->id);
                $booksa->sample_content = $books;

                $tempArray = json_decode($bookarray, true);
                //$tempArray2 = json_decode($booksa, true);

            }
            $bookarrays = $tempArray;
            return $bookarrays;
        }


    }

    public function bookcontentsample($booksid)
    {
            $bookimage = BooksImages::where('books_id', $booksid)->get();
            return $bookimage;


    }


}
