<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model {

    protected $table = 'dbp_user_level';
	protected $primaryKey = 'code';

}
