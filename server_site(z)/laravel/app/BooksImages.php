<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BooksImages extends Model {

    protected $table = 'dbp_books_images';
	protected $primaryKey = 'images_id';

	public function books()
    {
    	return $this->belongsTo('App\Books','id');

    }

}
