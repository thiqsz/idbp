<div style="font-size:12.8000001907349px;line-height:normal;direction:ltr;font-family:Open sans,Arial,sans-serif;padding:1.5em;border-radius:1em;max-width:580px;margin:8.671875px auto 0px"><br>
<table style="width:395px;background-image:initial;background-repeat:initial">
<tbody>
<tr>
<td style="font-family:arial,sans-serif;margin:0px">
<div style="width:90px;min-height:54px;margin:10px auto"><br>
</div>
<div style="width:350.09375px;padding-bottom:10px;padding-left:15px">
<p><span style="font-family:Open sans,Arial,sans-serif;font-weight:bold;font-size:small;line-height:1.4em">Hai {{$name}}</span></p>
<p><span style="font-family:Open sans,Arial,sans-serif;font-size:2.08em">Pembaharuan kata laluan.</span></p>
</div>
<div style="vertical-align:middle;padding:10px;max-width:398px;float:left">
<table style="vertical-align:middle">
<tbody>
<tr>
<td style="font-family:Open sans,Arial,sans-serif;margin:0px">
<br>
<span style="font-size:small;line-height:1.4em">Untuk memperbaharui kata laluan anda, klik pautan berikut dalam tempoh 24 jam dari tempoh email diterima.<br>
<a href="{{$link}}/proceed" style="color:rgb(17,85,204);text-decoration:none" target="_blank">{{$link}}/proceed</a>.</span>
<br><br>
<span style="font-size:small;line-height:1.4em">Klik pautan berikut untuk membatalkan permintaan pembaharuan kata laluan.<br>
<a href="{{$link}}/cancel" style="color:rgb(17,85,204);text-decoration:none" target="_blank">{{$link}}/cancel</a>.</span>
</td>
</tr>
</tbody>
</table>
</div>
<div style="float:left;clear:both;padding:0px 5px 10px 10px"></div>
<br>
<div style="float:left;clear:both;padding:0px 5px 10px 10px"></div>
<div style="vertical-align:middle;padding:10px;max-width:398px;float:left">
<table style="vertical-align:middle">
<tbody>
<tr>

</tr>
</tbody>
</table>
</div>
<br>
<br>
<div style="clear:both;padding-left:13px;min-height:6.8em">
<table style="width:376px;border-collapse:collapse;border:0px">
<tbody>
<tr>
<td style="margin:0px;width:68px"><img alt="iDBP icon"  src="https://lh4.googleusercontent.com/-RQ8jY7p7OEk/AAAAAAAAAAI/AAAAAAAAAAA/h2i2EChv8Wg/photo.jpg?sz=60" style="display:block" width="49"></td>
<td style="font-family:Open sans,Arial,sans-serif;margin:0px;vertical-align:bottom"><span style="font-size:small">Dewan Bahasa &amp; Pustaka,<br>
</span><font size="5"><span style="line-height: 24px;">iDBP</span></font></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<div style="line-height: normal; color: rgb(119, 119, 119); font-size: 0.8em; border-radius: 1em; padding: 1em; margin: 0px auto 17.34375px; font-family: Arial, Helvetica, sans-serif; text-align: center; background-color: rgb(229, 229, 229);">© 2015 iDBP, Dewan Bahasa &amp; Pustaka</div>
