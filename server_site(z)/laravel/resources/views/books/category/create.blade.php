<?php 
use Illuminate\html;
?>
      <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Kategori</h1>
      <h2 class="">Baru</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li><a href="{{ URL('books/book') }}">Kategori</a></li>
        <li class="active">Baru</li>
      </ol>
    </div>

  <div class="container clear_both padding_fix"> 


    <div id="main-content">
      <div class="page-content">
         </div>@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
        <div class="row">
          <div class="col-md-12">
<div class="block-web">
            <div class="header">
             <h3 class="content-header">Tambah Kategori</h3>
            </div>
            <div class="porlets-content">
             {!! Form::open(array('url' => 'books/category', 'class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data')) !!}
            
                <div class="form-group">
                    {!! Form::label('category_name', 'Nama Kategori', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::text('category_name', '', array('class' => 'form-control', 'id' => 'category_name')) !!}
                  </div>
                </div>
             <div class="bottom">
                 {!! Form::label('', '', array('class' => 'col-sm-2 control-label')) !!}
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="button" class="btn btn-default">Batal</button>
                </div><!--/form-group-->
              {!! Form::close() !!}
            </div>
          </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newBook(){
    window.location.href="{{ URL('books/book/create') }}";   
}
</script>