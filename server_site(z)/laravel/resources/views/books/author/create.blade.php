<?php 
use Illuminate\html;
?>
      <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Penulis</h1>
      <h2 class="">Baru</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li><a href="{{ URL('books/author') }}">Penulis</a></li>
        <li class="active">Baru</li>
      </ol>
    </div>

  <div class="container clear_both padding_fix"> 


    <div id="main-content">
      <div class="page-content">
         </div>@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
        <div class="row">
          <div class="col-md-12">
<div class="block-web">
            <div class="header">
              <h3 class="content-header">Tambah Penulis</h3>
            </div>
            <div class="porlets-content">
             {!! Form::open(array('url' => 'books/author', 'class' => 'form-horizontal row-border')) !!}
            
                <div class="form-group">
                    {!! Form::label('author_name', 'Nama Penulis', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::text('author_name', null, array('class' => 'form-control')) !!}
                  </div>
                </div>
             <div class="bottom">
                 {!! Form::label('', '', array('class' => 'col-sm-2 control-label')) !!}
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="button" class="btn btn-default" onclick="backbtn()">Batal</button>
                </div>
              {!! Form::close() !!}
            </div>
          </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function backbtn(){
    window.location.href="{{ URL('books/author') }}";   
}
</script>