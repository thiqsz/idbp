
  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku </h1>
      <h2 class="">Papar Buku</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li class="active">Papar Buku</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
      <div class="page-content">

        <div class="row">
          <div class="col-md-12">@if (Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
    </div>
           
            <div class="block-web">
              <div style="padding:0px;" class="header">
                <h3 style="background: #f2f2f2 url({{ URL::asset('images/grey.png') }});padding:30px 0px;text-align:center;font-weight:bold;" class="content-header">{{ strtoupper($books->title) }}</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group pull-right">
                     {!! link_to(URL::previous(), 'Kembali', ['class' => 'btn btn-danger','style' => 'padding-right:10px']) !!}

                    {!! link_to('books/book/'.$books->id.'/edit', 'Kemaskini', ['class' => 'btn btn-primary']) !!}
                    </div>

                  </div>
                  <div class="margin-top-10"></div>
                  <img width="35%" style="vertical-align:top;" src='{{ asset($books->pic) }}'>
                  <table style="display:inline-block;width:55%;margin-left:50px" class="table table-striped table-hover" id="editable-sample">
                    <tbody>
                      <tr>
                        <th class="col-sm-3 col-xs-3">Kategori</th>
                        <td class="col-sm-9 col-xs-9">{{implode(',',$books->categories)}}</td>
                      </tr>
                      <tr>
                        <th>Penulis</th>
                        <td>{{implode(',',$books->authors)}}</td>
                      </tr>
                      <tr>
                        <th>Bil Halaman</th>
                        <td>{{$books->pages}}</td>
                      </tr>
                      
                      <tr>
                        <th>Keterangan Produk</th>
                        <td>{{strip_tags($books->summary)}}</td>
                      </tr>
                    </tbody>
                  </table>
                   
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>