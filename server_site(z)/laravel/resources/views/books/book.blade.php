
  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Senarai Buku</h1>
      <h2 class="">Senarai Buku</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li class="active">Senarai Buku</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
      <div class="page-content">

        <div class="row">
          <div class="col-md-12">@if (Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
    </div>
           
            <div class="block-web">
              <div class="header">
                <h3 class="content-header">Senarai Buku</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button onclick="newBook()" class="btn btn-primary"> Tambah <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="btn-group pull-right">
                    
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                      <tr>
                        <th>Tajuk</th>
                        <th>Penulis</th>
                        <th class="center">Harga(RM)</th>
                        
          
                        <th class="center" class="center" class="center" class="center">
                        Kategori
                        </th>
                        <th class="center" class="center" class="center" class="center">
                        Tindakan
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($books as $value)
                    <tr>
                      <td>{{ $value->title }}</td>
                      <td>{{ implode(',',$value->authors)}}</td>
                      <td class="center">{{ number_format((float)$value->price, 2, '.', '') }}</td>
                      <td class="center">{{ implode(',',$value->categories)}}</td>
                      <td class="center">
                        {!! link_to_route('books.book.show', 'Lihat', array($value->id),array('class' => 'btn btn-xs btn-success','style'=>'margin-right:5px')) !!}
                        {!! link_to_route('books.book.edit', 'Kemaskini', array($value->id),array('class' => 'btn btn-xs btn-info','style'=>'margin-right:5px')) !!}
                        {!! Form::open(array('style' => 'display:inline-block','method'=> 'DELETE', 'url' =>array('books/book', $value->id))) !!}
                        {!! Form::submit('Padam', array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newBook(){
    window.location.href="{{ URL('books/book/create') }}";   
}

</script>