<?php 
use Illuminate\html;
?>
<style type="text/css">
    .multiselect{
          min-width: 220px;
          height: 30px;
    }
    .form-control{
          min-width: 65px;
    }
   </style>
      <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Ahli</h1>
      <h2 class="">Baru</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Pengguna</a></li>
        <li><a href="{{ URL('users/members') }}">Ahli</a></li>
        <li class="active">Baru</li>
      </ol>
    </div>

  <div class="container clear_both padding_fix"> 


    <div id="main-content">
      <div class="page-content">
         </div>@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
        <div class="row">
          <div class="col-md-8">
<div class="block-web">
            <div class="header">
              <h3 class="content-header">Tambah Ahli</h3>
            </div>
            <div class="porlets-content">
             {!! Form::open(array('url' => '/users/member', 'class' => 'form-horizontal row-border')) !!}
            
                <div class="form-group">
                    {!! Form::label('name', 'Nama', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
                  </div>
                </div>

                 <div class="form-group">
                    {!! Form::label('email', 'E-Mel', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
                  </div>


                </div>

                <div class="form-group">
                    {!! Form::label('password', 'Katalaluan (min 6 karakter)', array('class' => 'col-sm-2 control-label',)) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::password('password', null, array('class' => 'form-control')) !!}
                  </div>


                </div>


                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Pengesahan Katalaluan', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::password('password_confirmation', null, array('class' => 'form-control')) !!}
                  </div>


                </div>


             <div class="bottom">
                 {!! Form::label('', '', array('class' => 'col-sm-2 control-label')) !!}
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="button" class="btn btn-default" onclick="backbtn()">Batal</button>
                </div>
              {!! Form::close() !!}
            </div>
          </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function backbtn(){
    window.location.href="{{ URL('users/members') }}";   
}
</script>