define([
  'underscore',
  'backbone',
  'module/Model/BookModel',
], function(_, Backbone,Book) {

  var BookCollection = Backbone.Collection.extend({
      
       initialize: function(cat,start,except) {
           this.cat =cat;
           this.except = except;
//           this.start = start;
        
           if(typeof start !== 'number'){
              this.start = 0; 
           }else{
               this.start = start;
           }
//           console.log(parseInt(this.start)+'...'+cat);
  		},

      models:Book,

      url : function() {
          return  resource.rootUrl+resource.category+"?"+resource.prefixStart+this.start+resource.prefixCategory+this.cat
                  +resource.prefixExcept+this.except;
	    },
     
      parse: function(response) {
        console.log('data parsed');
        return response;
      },	
      
      sync: function(method, model, options){  
        options.timeout = 10000;  
        options.dataType = "jsonp"; 
        return Backbone.sync(method, model, options);  
      } 
     
	    
    });
    
    

  	return BookCollection;

});