// Filename: main.js
require.config({
    paths: {
        jquery: 'js/jquery-1.11.2.min',
        jqueryvalidate:'js/jquery.validate.min',     
        underscore: 'vendors/underscore-min',
        backbone: 'vendors/backbone-min',
        bootstrap:'vendors/bootstrap.min',
        blazy:'js/blazy.min',
        text:'vendors/text',
        router:'router',
        config:'config',
        fileDownload:'js/jquery.fileDownload'
        
    },
    shim: {
//        "jquery": {
//            deps:["base1"]
//        }
    },
    map: {
      "*": {
        "jquery": "noconflict"
      },
      "noconflict": {
        "jquery": "jquery"
      }
    }
    
   

});

require([

  // Load our app module and pass it to our definition function
  'app',
], function(App){
  // The "app" dependency is passed in as "App"
  App.initialize();
});