    // Filename: views/project/list
    define([
      'jquery',
      'underscore',
      'backbone',
      'config',
      'text!module/master/side/menu_template.html'
    ], function($, _, Backbone,Config,template){

        var MenuView = Backbone.View.extend({

            el:"#menu",
            
            events:{
                "click #test": "test",
                "click .menu_category": "menuCategory",  
                "click #emailMe":"gotoEmail",
                "click #gotoWeb":"gotoWeb",
            },

            initialize: function() {
             
            },
            
            test:function(){
              console.log("test MenuView");  
            },
            
            gotoRack:function(cat){
                  //got to rack
             console.log('sending ');
                sessionStorage.setItem('current-category',cat);
             Backbone.trigger('change-category');//Backbone.trigger('change-category', <CATEGORY> );
                    
            },
            gotoEmail:function(){
                window.open("mailto:"+resource.idbpEmail+"?subject=Hai iDBP", "_blank");
            },
            gotoWeb:function(){
                 navigator.app.loadUrl(resource.dbpWeb, { openExternal:true });
            },
            menuCategory:function(e){
                console.log($(e.currentTarget).data('cat'));
                mReset();
            
                this.gotoRack($(e.currentTarget).data('cat'));
            },


            render: function(){
              console.log("menu view rendered");
              var compiledTemplate = _.template(template);
              this.$el.html(compiledTemplate());
            },


      });

      return MenuView;
    });