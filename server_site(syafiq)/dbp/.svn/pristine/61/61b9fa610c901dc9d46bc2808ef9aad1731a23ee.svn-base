<?php namespace App\Http\Controllers;
use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use View;
use Response;
use File;
use Request;
class BooksBookController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $books = Books::leftJoin('dbp_category', 'dbp_books.cat_id', '=', 'dbp_category.cat_id')->leftJoin('dbp_authors', 'dbp_books.writer', '=', 'dbp_authors.author_id')->leftJoin('dbp_publishers', 'dbp_books.pub_id', '=', 'dbp_publishers.pub_id')->leftJoin('dbp_books_level', 'dbp_books.level', '=', 'dbp_books_level.level_id')->get();
		return View::make('index') 
    ->nest('header_script', 'header_script')
    ->nest('side_menu', 'side_menu')
    ->nest('body', 'books.book', array('books' => $books))
    ->nest('footer_script', 'footer_script_datatables');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		  $authors = Author::lists('author_name', 'author_id');
      $category = Category::lists('category_name', 'cat_id');
    	$booklevel = BookLevel::lists( 'level_name', 'level_id');
      $publishers = Publishers::lists('publisher_name', 'pub_id');
	    return View::make('index')  
      ->nest('header_script', 'header_script')
      ->nest('side_menu', 'side_menu')
      ->nest('body', 'books.book.create', array('authors' => $authors, 'category' => $category, 'booklevel' => $booklevel, 'publishers'=>$publishers))
      ->nest('footer_script', 'footer_script_forms');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'title'       => 'required',
            'price'      => 'required',
            'author' => 'required',
            'category' => 'required',
            'isbn' => 'required',
            'year' => 'required',
            'pages' => 'required',
            'booklevel' => 'required',
            'bookcover' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'summary' => 'required'
        );
            
      
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('books/book/create')
                ->withErrors($validator);
        } else {
            $book = new Books;
            if(Input::file('bookcover')->isValid()){
            $book->title  = Input::get('title');
            $book->price  = Input::get('price');
            $book->writer = Input::get('author');
            $book->cat_id = Input::get('category');
            $book->pub_id = Input::get('publisher');
            $book->isbn = Input::get('isbn');
            $book->level = Input::get('booklevel');
            $book->year = Input::get('year');
            $book->pages = Input::get('pages');
            $book->summary = Input::get('summary');
            $bookname = Input::file('bookcover')->getClientOriginalName();
            $titlename =   preg_replace('/[^A-Za-z0-9\-]/','',$book->title);
            $destinationPath = 'public/images/bookcover/'.$book->isbn.'-'.$titlename.'';
            $destinationPath2 = 'images/bookcover/'.$book->isbn.'-'.$titlename.'';
            $bookDestination = $destinationPath2.'/'.$bookname;
            if(!file::exists($destinationPath)){
            file::makeDirectory($destinationPath, 0777, true, true);
            Input::file('bookcover')->move($destinationPath, $bookname);
            }
            $book->pic = $bookDestination;
            $book->save();
            // redirect
            Session::flash('message', 'Successfully add new book!');
            return Redirect::to('books/book');
            }else{
            Session::flash('message', 'The uploaded images is invalid!');
            return Redirect::to('books/book/create');
            }
        }  

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the book
    $books = Books::find($id);
    if (is_null($books))
    {
        return Redirect::to('books/book');
    }
    $books = Books::leftJoin('dbp_authors', 'dbp_books.writer', '=', 'dbp_authors.author_id')
    ->leftJoin('dbp_category', 'dbp_books.cat_id', '=', 'dbp_category.cat_id')
    ->leftJoin('dbp_publishers', 'dbp_books.pub_id', '=', 'dbp_publishers.pub_id')
    ->leftJoin('dbp_books_level', 'dbp_books.level', '=', 'dbp_books_level.level_id')
    ->where('dbp_books.id','=',$id)
    ->first();
    // {
    // 	$join->on('dbp_books.writer','=','dbp_authors.author_id')
    // 	->where('dbp_books.id','=',24);
    // })


    // show the view and pass the books to it
    return View::make('index')
    ->nest('header_script', 'header_script')
    ->nest('side_menu', 'side_menu')
    ->nest('body', 'books.book.show', array('books' => $books))
    ->nest('footer_script', 'footer_script_forms');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$books = Books::find($id);
		if (is_null($books))
		{
		    return Redirect::to('books/book');
		}
				//redirect to update form
        $authors = Author::lists('author_name', 'author_id');
        $category = Category::lists('category_name', 'cat_id');
        $booklevel = BookLevel::lists( 'level_name', 'level_id');
        $publishers = Publishers::lists('publisher_name', 'pub_id');
	    return View::make('index')  
        ->nest('header_script', 'header_script')
        ->nest('side_menu', 'side_menu')
        ->nest('body', 'books.book.edit',  array('authors' => $authors, 'category' => $category, 'booklevel' => $booklevel, 'publishers'=>$publishers, 'books'=>$books))
        ->nest('footer_script', 'footer_script_forms');
	}
	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
					// validate
		        // read more on validation at http://laravel.com/docs/validation
		        $rules = array(
		        'title'       => 'required',
            'price'      => 'required',
            'author' => 'required',
            'category' => 'required',
            'isbn' => 'required',
            'year' => 'required',
            'pages' => 'required',
            'booklevel' => 'required',
            'bookcover' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'summary' => 'required'
		        );
		        $validator = Validator::make(Input::all(), $rules);

		        // process the login
		        if ($validator->fails()) {
		            return Redirect::to('books/book/' . $id . '/edit')
		                ->withErrors($validator);
		        } else {
		            // store
		            $book = Books::find($id);

		            $book->title  = Input::get('title');
		            $book->price  = Input::get('price');
		            $book->writer = Input::get('author');
		            $book->cat_id = Input::get('category');
		            $book->pub_id = Input::get('publisher');
		            $book->isbn = Input::get('isbn');
		            $book->level = Input::get('booklevel');
		            $book->year = Input::get('year');
		            $book->pages = Input::get('pages');
		            $book->summary = Input::get('summary');
		            $bookname = Input::file('bookcover')->getClientOriginalName();
		            $titlename =   preg_replace('/[^A-Za-z0-9\-]/','',$book->title);
		            $destinationPath = 'images/bookcover/'.$book->isbn.'-'.$titlename.'';
		            $bookDestination = $destinationPath.'/'.$bookname;
		            if(!File::exists($destinationPath)){
		            File::makeDirectory($destinationPath, 0777, true, true);
		            Request::File('bookcover')->move($destinationPath, $bookname);
		            }
		            $book->pic = $bookDestination;
		            $book->save();

		            // redirect
		            Session::flash('message', 'Successfully updated book!');
		            return Redirect::to('books/book');
		        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//delete books
    Books::find($id)->delete();
    return Redirect::to('books/book')
        ->withInput()
        ->with('message', 'Successfully deleted Book.');
	}
    
    

}
