

<div class="left_nav_slidebar">
        <ul>
          <li class="left_nav_active theme_border"><a href="{{ URL('/') }}"><i class="fa fa-home"></i> DASHBOARD <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
          </li>
          <li> <a href="javascript:void(0);"> <i class="fa fa-edit"></i> BUKU <span class="plus"><i class="fa fa-plus"></i></span></a>
            <ul>
              <li> <a href="{{ URL('books/book') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Senarai Buku</b> </a> </li>
              <li> <a href="{{ URL('books/category') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Kategori</b> </a> </li>
              <li> <a href="{{ URL('books/author') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Penulis</b> </a> </li>
              <li> <a href="{{ URL('books/publisher') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Penerbit</b> </a> </li>
              <li> <a href="{{ URL('books/level') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tahap</b> </a> </li>
            </ul>
          </li>
          <li> <a href="javascript:void(0);"> <i class="fa fa-tasks"></i> PENGGUNA <span class="plus"><i class="fa fa-plus"></i></span></a>
            <ul>
              <li> <a href="{{ URL('users/members') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Ahli</b> </a> </li>
              <li> <a href="{{ URL('users/staff') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Staff</b> </a> </li>
              <li> <a href="{{ URL('users/all') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Semua</b> </a> </li>
            </ul>
          </li>
          <li> <a href="javascript:void(0);"> <i class="fa fa-users icon"></i> TETAPAN <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul>
              <li> <a href="todo.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Maintenance</b> </a> </li>
            </ul>
          </li>
        </ul>
      </div>