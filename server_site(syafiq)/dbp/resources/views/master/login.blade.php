<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login | Dewan Bahasa &amp; Pustaka</title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('css/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('css/admin.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<div class="wrapper">
    <div class="login_page">
        <div class="login_content">
            <div class="panel-heading border login_heading">sign in now</div>
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">{!!$error!!}</p>
            @endforeach
            @if (Session::has('message'))
                <p class="alert alert-danger">{{ Session::get('message') }}</p>
            @endif
            {!!Form::open(['url'=>'/login','class'=>'form form-horizontal'])!!}
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="text" placeholder="Username" id="inputUsername" class="form-control" name="username">

                    </div>
                </div>
                <div class="form-group">

                    <div class="col-sm-10">
                        <input type="password" placeholder="Password" id="inputPassword3" class="form-control" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class=" col-sm-10">
                                <a class="btn btn-primary pull-left" href="{{ URL('/register') }}">Daftar Baru?</a>

                                <button class="btn btn-default pull-right" type="submit">Log Masuk</button>
                    </div>
                </div>

           {!!Form::close()!!}
        </div>
    </div>

<script src="{{ URL::asset('js/jquery-2.1.0.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/common-script.js') }}"></script>
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
</body>
</html>
