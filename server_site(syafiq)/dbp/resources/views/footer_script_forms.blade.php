<script src="{{ URL::asset('js/common-script.js') }}"></script>
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/toggle-switch/toggles.min.js') }}"></script>
<script src="{{ URL::asset('plugins/checkbox/zepto.js') }}"></script>
<script src="{{ URL::asset('plugins/checkbox/icheck.js') }}"></script>
<script src="{{ URL::asset('js/icheck-init.js') }}"></script>
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('js/icheck.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-fileupload/dropzone.js') }}?v=1423494334"></script>
<script src="{{ URL::asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/form-components.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/input-mask/jquery.inputmask.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-scroller/jquery.mCustomScrollbar.concat.min.js') }}"></script>
