
  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Users / All</h1>
      <h2 class="">Display All Users</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Users</a></li>
        <li class="active">All</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    <!--\\\\\\\ container  start \\\\\\-->
    
    <div id="main-content">
      <div class="page-content">
        <?php 
$alluser = DB::table('dbp_users')->leftJoin('dbp_user_status', 'dbp_users.status', '=', 'dbp_user_status.shortcode')->leftJoin('dbp_user_level', 'dbp_user_level.code', '=', 'dbp_users.level')->get();
?>
        <div class="row">
          <div class="col-md-12">
            <div class="block-web">
              <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Editable Table</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button id="editable-sample_new" class="btn btn-primary"> Add New <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="btn-group pull-right">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="#">Print</a></li>
                        <li><a href="#">Save as PDF</a></li>
                        <li><a href="#">Export to Excel</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th class="center">Status</th>
                        <th class="center" class="center">
                        User Type
                        </th>
                        <th class="center" class="center" class="center">
                        Registration Date
                        </th>
                        <th class="center" class="center" class="center" class="center">
                        Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($alluser as $value)
                    <tr>
                      <td>{{ $value->username }}</td>
                      <td>{{ $value->name }}</td>
                      <td class="center">{{ $value->fullcode }}</td>
                      <td class="center">{{ $value->username }}</td>
                      <td class="center">{{ date('d M Y', strtotime($value->dateRegistered)) }}</td>
                      <td class="center"><button class="btn btn-xs btn-info" style="margin-right: 5px;">Info</button>
                        <button class="btn btn-xs btn-danger">Delete</button></td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
