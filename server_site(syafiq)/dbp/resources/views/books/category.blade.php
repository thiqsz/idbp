
  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Kategori</h1>
      <h2 class="">Senarai Kategori</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li class="active">Kategori</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
      <div class="page-content">

        <div class="row">
          <div class="col-md-12">@if (Session::has('message'))
<div class="flash alert">{{ Session::get('message') }}</div>
@endif
    </div>
           
            <div class="block-web">
              <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Senarai Kategori</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button onclick="newCategory()" class="btn btn-primary"> Add New <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="btn-group pull-right">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="#">Print</a></li>
                        <li><a href="#">Save as PDF</a></li>
                        <li><a href="#">Export to Excel</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                      <tr>
                        <th>Category</th>
                        <th class="center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($category as $value)
                    <tr>
                      <td>{{ $value->category_name }}</td>
                      <td class="center">
                      {!! link_to_route('books.category.edit', 'Update', array($value->cat_id),
                                      array('class' => 'btn btn-xs btn-info')) !!}
                        {!! Form::open(array('style' => 'display:inline-block','method'=> 'DELETE', 'url' =>array('books/category', $value->cat_id))) !!}
                        {!! Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newCategory(){
    window.location.href="{{ URL('books/category/create') }}";
}
function updateBook(val){
      window.location.href="{{ URL('books/book') }}/"+val+"/edit";  
}
</script>