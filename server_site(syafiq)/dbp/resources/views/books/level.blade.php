  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Peringkat</h1>
      <h2 class="">Senarai Semua Peringkat</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li class="active">Peringkat</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
    @if ( Session::has('flash_message') )
      <div style="padding:5px;margin-bottom:0px;" class="alert {{ Session::get('flash_type') }}">
          <h3 style="font-size:20px;">{{ Session::get('flash_message') }}</h3>
      </div>
      
    @endif
      <div class="page-content">
      
                <!-- @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif -->
        <div class="row">
          <div class="col-md-12">
          
    </div>
           
            <div class="block-web">
              <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Senarai Peringkat</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button onclick="newlevel()" class="btn btn-primary"> Tambah <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="btn-group pull-right">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="#">Print</a></li>
                        <li><a href="#">Save as PDF</a></li>
                        <li><a href="#">Export to Excel</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                      <tr>
                        <th>Peringkat</th>
                        <th class="center">Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($level as $value)
                    <tr>
                      <td>{{ $value->level_name }}</td>
                      <td class="center">
                        {!! link_to_route('books.level.edit', 'Kemaskini', array($value->level_id),array('class' => 'btn btn-xs btn-info')) !!}
                        {!! Form::open(array('style' => 'display:inline-block','method'=> 'DELETE', 'url' =>array('books/level', $value->level_id))) !!}
                        {!! Form::submit('Padam', array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newlevel(){
    window.location.href="{{ URL('books/level/create') }}";
}
</script>