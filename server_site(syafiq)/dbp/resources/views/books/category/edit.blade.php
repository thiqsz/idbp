<?php 
use Illuminate\html;
?>
      <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Books / Category / Update</h1>
      <h2 class="">Update Category</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Books</a></li>
        <li><a href="{{ URL('books/category') }}">Category</a></li>
        <li class="active">Update</li>
      </ol>
    </div>

  <div class="container clear_both padding_fix"> 


    <div id="main-content">
      <div class="page-content">
         </div>@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
        <div class="row">
          <div class="col-md-12">
<div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Update {{$category->category_name}}</h3>
            </div>
            <div class="porlets-content">
            {!! Form::model($category, array('route' => array('books.category.update', $category->cat_id), 'method' => 'PUT','class'=>'form-horizontal row-border')) !!}
             <!-- {!! Form::open(array('url' => 'books/category', 'class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data')) !!} -->
            
                <div class="form-group">
                    {!! Form::label('category_name', 'Category Name', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::text('category_name', null, array('class' => 'form-control')) !!}
                  </div>
                </div>
             <div class="bottom">
                 {!! Form::label('', '', array('class' => 'col-sm-2 control-label')) !!}
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                </div><!--/form-group-->
              {!! Form::close() !!}
            </div>
          </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newBook(){
    window.location.href="{{ URL('books/book/create') }}";   
}
</script>