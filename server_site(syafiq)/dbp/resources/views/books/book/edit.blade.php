<div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
        <h1>Buku / Senarai Buku / Kemaskini</h1>

        <h2 class="">Kemaskini Buku</h2>
    </div>
    <div class="pull-right">
        <ol class="breadcrumb">
            <li><a href="#">Buku</a></li>
            <li><a href="{{ URL('books/book') }}">Senarai Buku</a></li>
            <li class="active">Kemaskini</li>
        </ol>
    </div>

    <div class="container clear_both padding_fix">


        <div id="main-content">
            <div class="page-content">
            </div>@if($errors->any())
                <div class="alert alert-danger">{{$errors->first()}}</div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="block-web">
                        <div class="header">
                            <div class="actions"><a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a
                                        class="close-down" href="#"><i class="fa fa-times"></i></a></div>
                            <h3 class="content-header">Kemaskini Buku</h3>
                        </div>
                        <div class="porlets-content">
                            {!! Form::model($books, array('route' => array('books.book.update', $books->id), 'method' =>
                            'PUT', 'id'=>'fileupload','class'=>'form-horizontal row-border','enctype' =>
                            'multipart/form-data')) !!}


                            <div class="form-group">
                                {!! Form::label('title', 'Tajuk', array('class' => 'col-sm-2 control-label')) !!}

                                <div class="col-sm-5">
                                    {!! Form::text('title', null, array('class' => 'form-control', 'id' =>
                                    'form-title')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('price', 'Harga', array('class' => 'col-sm-2 control-label')) !!}

                                <div class="col-sm-1">
                                    {!! Form::text('price', null, array('class' => 'form-control', 'id' =>
                                    'form-price')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('author', 'Penulis', array('class' => 'col-sm-2 control-label')) !!}
                                <div class="col-sm-2">
                                    {!! Form::select('author', $authors, $books->auth_id, array('class' =>
                                    'form-control')) !!}
                                </div>
                                <a data-toggle="modal" data-target="#addAuthor" class="btn btn-s btn-info"><i
                                            class="fa fa-plus-square"></i> {{ 'Tambah Penulis' }}</a>
                            </div>
                            <div class="form-group">
                                {!! Form::label('category', 'Kategori', array('class' => 'col-sm-2 control-label')) !!}
                                <div class="col-sm-2">
                                    {!! Form::select('category', $category, $books->cat_id, array('class' =>
                                    'form-control')) !!}
                                </div>
                                <a data-toggle="modal" data-target="#addCategory" class="btn btn-s btn-info"><i
                                            class="fa fa-plus-square"></i> {{ 'Tambah Kategori' }}</a>
                            </div>
                            <div class="form-group">
                                {!! Form::label('booklevel', 'Tahap Buku', array('class' => 'col-sm-2 control-label'))
                                !!}
                                <div class="col-sm-2">
                                    {!! Form::select('booklevel', $booklevel, $books->level, array('class' =>
                                    'form-control')) !!}
                                </div>
                                <a data-toggle="modal" data-target="#addBookLevel" class="btn btn-s btn-info"><i
                                            class="fa fa-plus-square"></i> {{ 'Tambah Tahap Buku' }}</a>
                            </div>
                            <div class="form-group">
                                {!! Form::label('publisher', 'Penerbit', array('class' => 'col-sm-2 control-label')) !!}
                                <div class="col-sm-2">

                                    {!! Form::select('publisher', $publishers, $books->pub_id, array('class' =>
                                    'form-control', 'id'=>'form-pub')) !!}
                                </div>
                                <a data-toggle="modal" data-target="#addPublisher" class="btn btn-s btn-info"><i
                                            class="fa fa-plus-square"></i> {{ 'Tambah Penerbit' }}</a>
                            </div>
                            <div class="form-group">
                                {!! Form::label('isbn', 'ISBN', array('class' => 'col-sm-2 control-label')) !!}

                                <div class="col-sm-2">
                                    {!! Form::text('isbn', null, array('class' => 'form-control', 'id' => 'form-isbn'))
                                    !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('year', 'Tahun Penerbitan', array('class' => 'col-sm-2 control-label'))
                                !!}

                                <div class="col-sm-1">
                                    {!! Form::text('year', null, array('class' => 'form-control', 'id' => 'form-year'))
                                    !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('pages', 'Jumlah Mukasurat', array('class' => 'col-sm-2 control-label'))
                                !!}

                                <div class="col-sm-1">
                                    {!! Form::text('pages', null, array('class' => 'form-control', 'id' =>
                                    'form-pages')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('bookcover', 'Muatnaik Muka Depan', array('class' => 'col-sm-2
                                control-label')) !!}

                                <div class="col-sm-3">
                                    @if(isset($books->pic))
                                        <div class="cover-img">
                                            <a href="{{ URL($books->pic) }}" target="_blank"><img src="{{ URL($books->pic) }}" alt="{{ $books->title }}"></a>
                                        </div>
                                    @endif
                                    {!! Form::file('bookcover', array('class' => 'form-control', 'id' =>
                                    'form-bookcover')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('bookcontentepub', 'Muatnaik Kandungan Buku', array('class' => 'col-sm-2
                                control-label')) !!}
                                <div class="col-sm-3">
                                    @if(isset($books->epub))
                                    <div class="epub-link">
                                    <a href="{{ URL($books->epub) }}" target="_blank"><div class="epub-icon"></div></a>
                                    </div>
                                    @endif
                                    {!! Form::file('bookcontentepub', '', array('class' => 'form-control', 'id' =>
                                    'form-bookcontentepub')) !!}
                                </div>
                                <i>Format: .epub</i>
                            </div>
                            <div class="form-group">
                                {!! Form::label('bookcover', 'Muatnaik Contoh Kandungan', array('class' => 'col-sm-2
                                control-label')) !!}
                                <div class=" col-sm-7">

                                <div class="booksample">

                                    <ul class="bookcontentlis">
                                        @foreach($bookimages as $bookimage)
                                            <li><img src="{{ URL::asset($bookimage->images_path) }}" height="214"></li>
                                        @endforeach
                                    </ul>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-6">
                                    <div id="mydropzone" class="dropzone"></div>
                                </div>
                            </div>


                            <div class="form-group">
                                {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
                                <div class="col-sm-2">
                                    {!! Form::select('status', $status, $books->status, array('class' =>
                                    'form-control')) !!}
                                </div>
                            </div>


                            <div class="form-group">
                                {!! Form::label('summary', 'Sinopsis', array('class' => 'col-sm-2 control-label')) !!}

                                <div class="col-sm-7">
                                    {!! Form::textarea('summary', null, array('class' => 'form-control ckeditor', 'id'
                                    => 'form-text')) !!}
                                </div>
                            </div>
                            <div class="bottom">
                                {!! Form::label('', '', array('class' => 'col-sm-2 control-label')) !!}
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="button" class="btn btn-default" onclick="backbtn()">Batal</button>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<div class="modal fade" id="addAuthor" tabindex="-1" role="dialog" aria-labelledby="addAuthor" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Penulis</h4>
            </div>
            {!! Form::open(array('url' => 'books/author', 'class' => 'ajax-addauth form-horizontal row-border')) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('author_name', 'Nama Penulis', array('class' => 'col-sm-3 control-label')) !!}

                    <div class="col-sm-8">
                        {!! Form::text('author_name', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" id="valajaxaddauth" class="btn btn-primary">Simpan</button>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="Kategori" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Kategori</h4>
            </div>
            {!! Form::open(array('url' => 'books/category', 'class' => 'ajax-addcat form-horizontal row-border',
            'enctype' => 'multipart/form-data')) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('category_name', 'Nama Kategori', array('class' => 'col-sm-3 control-label')) !!}

                    <div class="col-sm-8">
                        {!! Form::text('category_name', '', array('class' => 'form-control', 'id' => 'category_name'))
                        !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" id="valajaxaddcat" class="btn btn-primary">Simpan</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="addBookLevel" tabindex="-1" role="dialog" aria-labelledby="Tahap" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Tahap Buku</h4>
            </div>
            {!! Form::open(array('url' => 'books/level', 'class' => 'ajax-addlevel form-horizontal row-border',
            'enctype' => 'multipart/form-data')) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('level_name', 'Peringkat', array('class' => 'col-sm-3 control-label')) !!}

                    <div class="col-sm-8">
                        {!! Form::text('level_name', '', array('class' => 'form-control', 'id' => 'level_name')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" id="valajaxaddlevel" class="btn btn-primary">Simpan</button>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="addPublisher" tabindex="-1" role="dialog" aria-labelledby="Penerbit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Penerbit</h4>
            </div>
            {!! Form::open(array('url' => 'books/publisher', 'class' => 'ajax-addpub form-horizontal row-border',
            'enctype' => 'multipart/form-data')) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('publisher_name', 'Nama Penerbit', array('class' => 'col-sm-3 control-label')) !!}

                    <div class="col-sm-8">
                        {!! Form::text('publisher_name', '', array('class' => 'form-control', 'id' => 'publisher_name'))
                        !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" id="valajaxaddpub" class="btn btn-primary">Simpan</button>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script>

    function backbtn() {
        window.location.href = "{{ URL('books/book') }}";
    }

    $(document).ready(function () {

        $("#form-price, #form-isbn, #form-pages, #form-year").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
    (function ($) {
        $.fn.currencyFormat = function () {
            this.each(function (i) {
                $(this).change(function (e) {
                    if (isNaN(parseFloat(this.value))) return;
                    this.value = parseFloat(this.value).toFixed(2);
                });
            });
            return this;
        }

    })(jQuery);
    $(function () {
        $('#form-price').currencyFormat();

    });
    $(document).ready(function () {
        $("#form-year").attr('maxlength', '4');
        $(".booksample").mCustomScrollbar({
            "axis": "x",
            "theme": "dark",
            "autoExpandScrollbar": true,
            "advanced": {"autoExpandHorizontalScroll": true}
        });
    });

    $(document).ready(function () {
        $("button#valajaxaddauth").click(function () {
            $.ajax({
                type: "POST",
                url: "{{ URL('books/author') }}", //process to mail
                data: $('form.ajax-addauth').serialize(),
                dataType: "json",
                success: function (result) {
                    $("#author").append('<option value=\'' + result.authorid + '\'>' + result.authorname + '</option>');//hide button and show thank you
                    $("#addAuthor").modal('hide'); //hide popup
                },
                error: function () {
                    alert("Gagal Menyimpan Penulis Baru");
                }
            });
        });
        $("button#valajaxaddcat").click(function () {
            $.ajax({
                type: "POST",
                url: "{{ URL('books/category') }}", //process to mail
                data: $('form.ajax-addcat').serialize(),
                success: function (result) {
                    $("#category").append('<option value=\'' + result.catid + '\'>' + result.catname + '</option>');//hide button and show thank you
                    $("#addCategory").modal('hide'); //hide popup
                },
                error: function () {
                    alert("Gagal Menyimpan Kategori Baru");
                }
            });
        });
        $("button#valajaxaddlevel").click(function () {
            $.ajax({
                type: "POST",
                url: "{{ URL('books/level') }}", //process to mail
                data: $('form.ajax-addlevel').serialize(),
                success: function (result) {
                    $("#booklevel").append('<option value=\'' + result.levelid + '\'>' + result.levelname + '</option>');//hide button and show thank you
                    $("#addBookLevel").modal('hide'); //hide popup
                },
                error: function () {
                    alert("Gagal Menyimpan Tahap Buku");
                }
            });
        });
        $("button#valajaxaddpub").click(function () {
            $.ajax({
                type: "POST",
                url: "{{ URL('books/publisher') }}", //process to mail
                data: $('form.ajax-addpub').serialize(),
                success: function (result) {
                    $("#form-pub").append('<option value=\'' + result.pubid + '\'>' + result.pubname + '</option>');//hide button and show thank you
                    $("#addPublisher").modal('hide'); //hide popup
                },
                error: function () {
                    alert("Gagal Menyimpan Tahap Buku");
                }
            });
        });
    });

</script>


<script>
    jQuery(document).ready(function () {
        Dropzone.autoDiscover = false;
        $('div#mydropzone').dropzone({ // The camelized version of the ID of the form element

            // The configuration we've talked about above
            paramName: 'files',
            autoProcessQueue: false,
            previewsContainer: '.dropzone',
            uploadMultiple: true,
            parallelUploads: 25,
            maxFiles: 25,
            url: $('#fileupload').attr('action'),


        });
    });

</script>
