<script src="{{ URL::asset('js/common-script.js') }}"></script>
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.sparkline.js') }}"></script>
<script src="{{ URL::asset('js/sparkline-chart.js') }}"></script>
<script src="{{ URL::asset('js/graph.js') }}"></script>
<script src="{{ URL::asset('js/edit-graph.js') }}"></script>
<script src="{{ URL::asset('plugins/kalendar/kalendar.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/kalendar/edit-kalendar.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/sparkline/jquery.sparkline.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/sparkline/jquery.customSelect.min.js') }}" ></script> 
<script src="{{ URL::asset('plugins/sparkline/sparkline-chart.js') }}"></script> 
<script src="{{ URL::asset('plugins/sparkline/easy-pie-chart.js') }}"></script>
<script src="{{ URL::asset('plugins/morris/morris.min.js') }}" type="text/javascript"></script> 
<script src="{{ URL::asset('plugins/morris/raphael-min.js') }}" type="text/javascript"></script>  
<script src="{{ URL::asset('plugins/morris/morris-script.js') }}"></script> 
<script src="{{ URL::asset('plugins/demo-slider/demo-slider.js') }}"></script>
<script src="{{ URL::asset('plugins/knob/jquery.knob.min.js') }}"></script> 
<script src="{{ URL::asset('js/jPushMenu.js') }}"></script> 
<script src="{{ URL::asset('js/side-chats.js') }}"></script>
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('plugins/scroll/jquery.nanoscroller.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-scroller/jquery.mCustomScrollbar.concat.min.js') }}"></script>
