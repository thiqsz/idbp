<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbpUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dbp_users', function(Blueprint $table)
		{
            $table->increments('user_id');
            $table->string('username');
            $table->string('name');
            $table->string('email')->unique();;
            $table->string('password', 64);
            $table->string('level');
            $table->string('status');
            $table->longText('profile_photo');
            $table->longText('ip_address');
            $table->timestamp('dateRegistered');
            $table->timestamp('dateUpdated');
            $table->timestamps('lastLogin');
            $table->string('remember_token',100)->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dbp_users');
	}

}
