<?php namespace App\Http\Controllers;

use DB;
use View;
use Response;

class UsersAllController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		    return View::make('index')  
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'users_all')
            ->nest('footer_script', 'footer_script_datatables');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		    return View::make('index')  
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'users_all_add')
            ->nest('footer_script', 'footer_script');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
    
    

}
