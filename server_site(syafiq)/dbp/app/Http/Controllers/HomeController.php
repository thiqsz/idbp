<?php namespace App\Http\Controllers;
use Session;
use Validator;
use Input;
use Redirect;
use View;
use Response;
use File;
use Request;
use Auth;
use App\DBPUsers;
use Hash;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
    public function showRegister()
    {
        if(Auth::check()){
            return Redirect::to('dashboard');
        }else {
            return View::make('master.register');
        }
    }

    public function toRegister(){
        $rules = array(
            'username'    => 'required|min:5',
            'name'   => 'required',
            'email'=>'required|email',// make sure the email is an actual email
            'password'=>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation'=>'required|alpha_num|between:6,12'
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $user = new DBPUsers();
            $user->username = Input::get('username');
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            return Redirect::to('login')->with('message', 'Terima Kasih Kerana Mendaftar!');
        } else {

        }
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

        }
    }
    public function showLogin()
    {

        if(Auth::check()){
            return Redirect::to('dashboard');
        }else{
        return View::make('master.login');
        }
    }

    public function doLogin()
    {

        $rules = array(
            'username'    => 'required',
            'password' => 'required|alphaNum|min:5'
        );

        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
                dd($validator);
        } else {
        
            $userdata = array(
                'username'     => Input::get('username'),
                'password'  => Input::get('password')
            );


            if (Auth::attempt($userdata)) {
                return Redirect::to('dashboard');
            } else {
                return Redirect::to('login')->with('message', 'Nama Pengguna/Kata Haluan Tidak Sah. Sila Cuba Lagi')->withInput();
            }

        }
    }
    public function doLogout()
    {
        Auth::logout();
        return Redirect::to('login');
    }

}
